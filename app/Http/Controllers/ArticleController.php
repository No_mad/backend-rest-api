<?php

namespace App\Http\Controllers;

use App\Http\Resources\ArticleCollection;
use App\Http\Resources\ArticleResource;
use App\Services\ArticleService;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    public function __construct(ArticleService $articleService)
    {
        $this->articleService = $articleService;
    }

    public function getArticles()
    {
        return new ArticleCollection($this->articleService->all());
    }

    public function getArticle(int $id)
    {
        if ($this->articleService->isRecordExists($id)) {
            return ([
                'collection' => new ArticleResource($this->articleService->findById($id)),
                'success' => true
            ]);
        }
        return response()->json([
            'success' => false,
            'msg' => 'Record doesn\'t exist'
        ]);
    }

    public function getArticleByTitle(Request $request)
    {
        $request->validate([
            'titleString' => 'required'
        ]);

        $article = $this->articleService->findByTitle($request->titleString);
        if ($article) {
            return new ArticleCollection($article);
        }
        return response()->json([
            'success' => false,
            'msg' => 'Record doesn\'t exist',
        ]);
    }

    public function createArticle(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'body' => 'required'
        ]);

        $this->articleService->create([
            'title_articles' => $request->title,
            'body_articles' => $request->body
        ]);
        return response()->json([
            'success' => true
        ]);
    }

    public function deleteArticle(int $id)
    {
        if ($this->articleService->isRecordExists($id)) {
            $this->articleService->destroy($id);
            return response()->json([
                'success' => true
            ]);
        }
        return response()->json(['success' => false, 'msg' => 'Record doesn\'t exist']);
    }

    public function updateArticle(int $id, Request $request)
    {
        if ($this->articleService->isRecordExists($id)) {
            $request->validate([
                'title' => 'required',
                'body' => 'required'
            ]);

            $this->articleService->update($id, [
                'title_articles' => $request->title,
                'body_articles' => $request->body
            ]);

            return response()->json([
                'success' => true
            ]);
        }
        return response()->json(['success' => false, 'msg' => 'Record doesn\'t exist']);
    }
}
