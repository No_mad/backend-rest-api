<?php

namespace App\Http\Middleware;

use App\Models\Payment;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class LogRequestResponse
{
    /**`
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $response = $next($request);

        Log::info("Logging incoming request and server response:",
            [
                "\nFrom:" => $request->ip(),
                "\nTo:" => $request->fullUrl(),
                "\nrequest:" => $request->all(),
                "\nresponse:" => $response
            ]);

        $payment = Payment::where([
            'ip_payments' => $request->ip(),
            'method_payments' => $request->fullUrl()
        ])->first();
        if ($payment) {
            $payment->increment('accessed_payments');
        }else{
            Payment::create([
                'ip_payments' => $request->ip(),
                'accessed_payments' => 1,
                'method_payments' => $request->fullUrl()
            ]);
        }

        return $response;
    }
}
