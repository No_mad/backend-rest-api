<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    use HasFactory;

    protected $primaryKey = 'id_articles';
    public $timestamps = false;
    protected $fillable = [
        'title_articles',
        'body_articles'
    ];
}
