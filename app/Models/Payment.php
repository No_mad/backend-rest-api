<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $primaryKey = 'id_payments';
    protected $fillable = [
        'ip_payments',
        'accessed_payments',
        'method_payments'
    ];
}
