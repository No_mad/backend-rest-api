<?php

namespace App\Repositories;

use App\Models\Article;



class ArticleRepository extends BaseRepository
{

    public function __construct(Article $model)
    {
        $this->model = $model;
    }

    public function isRecordExists(int $id)
    {
        return $this->model->find($id)->exists();
    }

    public function findByTitle(string $title)
    {
        $article = $this->model->where('title_articles', $title)->orWhere('title_articles', 'like',
            '%' . $title . '%')->paginate(3);
        if ($article->first()) {
            return $article;
        }
        return null;
    }
}
