<?php

namespace App\Services;

use App\Repositories\ArticleRepository;

class ArticleService extends BaseService
{
    public function __construct(ArticleRepository $repo)
    {
        $this->repo = $repo;
    }

    public function isRecordExists(int $id)
    {
        return $this->repo->isRecordExists($id);
    }

    public function findByTitle(string $title){
        return $this->repo->findByTitle($title);
    }
}
