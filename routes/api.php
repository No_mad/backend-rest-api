<?php

use App\Http\Controllers\ArticleController;
use App\Http\Controllers\AuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::get('/user', function (Request $request) {
    return $request->user();
});*/

Route::post('register', [AuthController::class, 'register']);
Route::post('login', [AuthController::class, 'login'])
    ->name('login');

Route::get('articles', [ArticleController::class, 'getArticles'])
    ->middleware('auth:sanctum');
Route::get('article/{id}', [ArticleController::class, 'getArticle'])
    ->where('id', '[0-9]+')
    ->middleware('auth:sanctum');
Route::post('article/title', [ArticleController::class, 'getArticleByTitle'])
    ->middleware('auth:sanctum');
Route::delete('article/{id}', [ArticleController::class, 'deleteArticle'])
    ->where('id', '[0-9]+')
    ->name('deleteArticle')
    ->middleware('auth:sanctum');
Route::post('article/title/body', [ArticleController::class, 'createArticle'])
    ->middleware('auth:sanctum');
Route::put('article/{id}/title/body', [ArticleController::class, 'updateArticle'])
    ->where('id', '[0-9]+')
    ->middleware('auth:sanctum');
