<?php

namespace Tests\Unit;

use App\Http\Resources\ArticleCollection;
use App\Models\Article;
use App\Models\User;
use Illuminate\Http\Request;
use Tests\TestCase;


class ArticleTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_get_articles()
    {
        $user = User::find(5);
        $this->actingAs($user);

        $response = $this->get('/api/articles');

        $response->assertStatus(200);
        $response->assertJson([
            'data' => true
        ]);
    }

    public function test_get_article()
    {
        $user = User::find(5);
        $this->actingAs($user);


        $this->post('api/article/title/body', [
            'title' => 'segaweg',
            'body' => 'segsegseg'
        ]);

        $lastArticle = Article::all()->last();

        $response = $this->get('/api/article/'.$lastArticle->id_articles);

        $response->assertStatus(200);
        $response->assertJson([
            'data' => true
        ]);

        $this->delete(route('deleteArticle', ['id' => $lastArticle->id_articles]));
    }

    public function test_create_article_authentication()
    {
        $response = $this->post('/api/article/title/body', [
            'title' => 'sfegesg',
            'body' => 'sefageagr'
        ]);

        $response->assertStatus(302);
    }

    public function test_delete_article_authentication()
    {
        $response = $this->delete('/api/article/1');

        $response->assertStatus(302);
    }

    public function test_update_article_authentication()
    {
        $response = $this->put('/api/article/1/title/body', [
            'title' => 'sfegesg',
            'body' => 'sefageagr'
        ]);

        $response->assertStatus(302);
    }

    public function test_create_article()
    {
        $user = User::find(5);

        $this->actingAs($user);

        $response = $this->post('api/article/title/body', [
            'title' => 'segaweg',
            'body' => 'segsegseg'
        ]);

        $response->assertOk();
    }

    public function test_delete_article()
    {
        $user = User::find(5);
        $this->actingAs($user);


        $response = $this->post('api/article/title/body', [
            'title' => 'segaweg',
            'body' => 'segsegseg'
        ]);

        $lastArticle = Article::all()->last();

        $response = $this->delete(route('deleteArticle', ['id' => $lastArticle->id_articles]));

        $response->assertOk();
    }

    public function test_update_article()
    {
        $user = User::find(5);
        $this->actingAs($user);

        $this->post('api/article/title/body', [
            'title' => 'segaweg',
            'body' => 'segsegseg'
        ]);

        $lastArticle = Article::all()->last();

        $response = $this->put('api/article/' . $lastArticle->id_articles . '/title/body', [
            'title' => 'title updated by test',
            'body' => 'body updated by test'
        ]);

        $response->assertOk();

        $this->delete(route('deleteArticle', ['id' => $lastArticle->id_articles]));
    }
}
